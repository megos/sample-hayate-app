using Xunit;

namespace SampleApp.Core
{
    public class WelcomeMessageTest
    {
        private WelcomeMessage _target;

        public WelcomeMessageTest()
        {
            _target = new WelcomeMessage();
        }

        [Fact]
        public void Text()
        {
            var actual = _target.Text;
            
            Assert.Equal("Welcome to the GitLab CI / CD world!", actual);
        }
    }
}