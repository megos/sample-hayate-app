namespace SampleApp.Core
{
    public sealed class WelcomeMessage
    {
        public string Text { get; } = "Welcome to the GitLab CI / CD world!";
    }
}